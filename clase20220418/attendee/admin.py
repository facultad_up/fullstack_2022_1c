from django.contrib import admin

from attendee.models import Attendee


admin.site.register(Attendee)
