from datetime import datetime
from django.db import models


class Event(models.Model):
    location = models.CharField(max_length=200)
    date = models.DateTimeField(default=datetime.now())

    