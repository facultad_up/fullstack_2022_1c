from datetime import time
from flask_restx import fields


class CustomFieldTime(fields.Raw):
    """Return a formatted time string in H:M"""
    
    __schema_type__ = 'string'
    __schema_format__ = 'time'

    def __init__(self, time_format='%H:%M', **kwargs):
        super().__init__(**kwargs)
        self.time_format = time_format

    def format(self, value):
        """Generate the string. for example to use in the response"""
        try:
            value = self.parse(value)
            return value.strftime(self.time_format)
        except (AttributeError, ValueError) as e:
            raise fields.MarshallingError(e)

    def parse(self, value):
        """Generate a time from the string in value"""
        if isinstance(value, time):
            return value

        if isinstance(value, str):
            return time.fromisoformat(value)

        raise ValueError('Unsupported Time Format')

    