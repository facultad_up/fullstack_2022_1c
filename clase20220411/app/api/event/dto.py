from flask_restx import Namespace, fields

from app.common.dto import CustomFieldTime


api = Namespace(
    'Event',
    path='/event',
    description='Event related operations'
)

event = api.model(
    'Event',
    {
        'id': fields.Integer(
            required=True,
            description="Event's identifier"
        ),
        'description': fields.String(
            required=True,
            description='A descr'
        ),
        'start_date': fields.Date(
            required=True,
            description='When'
        ),
        'start_time': CustomFieldTime(
            required=True,
            description='Time in 24 hours format',
            example='19:21'
        ),
        'location': fields.String(
            required=True,
            description='A descr'
        )
    }
)

event_list = api.model(
    'EventList',
    {
        'events': fields.List(fields.Nested(event), required=True)
    }
)

error_response = api.model(
    'ErrorResponse',
    {
        'code': fields.String,
        'message': fields.String
    }
)
