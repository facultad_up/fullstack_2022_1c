from datetime import datetime
from app.models.event import Event
from app.models.schemas import EventSchema


class EventService:
    @staticmethod
    def get_events(starts_at: datetime, ends_at:datetime):
        from_date = starts_at.date()
        from_time = starts_at.time()

        to_date = ends_at.date()
        to_time = ends_at.time()

        query = Event.query.filter(
            ((Event.start_date > from_date) |
             ((Event.start_date == from_date) & (Event.start_time >= from_time))) &
            ((Event.start_date < to_date) |
             ((Event.start_date == to_date) & (Event.start_time <= to_time)))
        ).all()

        data = EventSchema(many=True).dump(query)

        return data
