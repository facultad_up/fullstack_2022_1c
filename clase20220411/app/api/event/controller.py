from flask_restx import (
    Resource,
    reqparse,
    inputs
)
from flask import (
    abort,
    jsonify
)

from .service import EventService
from .dto import (
    api as event_api,
    event as event_dto,
    event_list as event_list_dto,
    error_response as error_response_dto
)


parser = reqparse.RequestParser()
parser.add_argument(
    'starts_at',
    type=inputs.datetime_from_iso8601,
    help='Return the events that starts after this date'
)
parser.add_argument(
    'ends_at',
    type=inputs.datetime_from_iso8601,
    help='Return the events that ends before this date'
)


@event_api.route('/')
class EventList(Resource):
    @event_api.doc(
        'Get all events between dates'
    )
    @event_api.expect(parser)
    @event_api.response(
        200,
        description='List of events',
        model=event_list_dto,
        envelope='data'
    )
    @event_api.response(
        400,
        description='The request was not correctly formed...',
        model=error_response_dto,
        envelope='error'
    )
    @event_api.response(
        500,
        description='Huge fail',
        model=error_response_dto,
        envelope='error'
    )
    def get(self):
        """Lists events"""
        try:
            args = parser.parse_args()

            if not args['starts_at']:
                abort(400, 'Missing starts_at argument')

            if not args['ends_at']:
                abort(400, 'Missing ends_at argument')

        except Exception as e:
            abort(400, str(e))

        starts_at = args['starts_at']
        ends_at = args['ends_at']

        try:
            events = EventService.get_events(starts_at, ends_at)
        except Exception as e:
            abort(500, str(e))

        return jsonify(
            {
                'data': {
                    'events': events
                }
            }
        )
