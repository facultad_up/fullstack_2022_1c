from app import ma

from app.models.event import Event


class EventSchema(ma.Schema):
    class Meta:
        model = Event
        fields = (
            'id', 'description',
            'start_date', 'start_time',
            'location'
        )
