from app import db


class Event(db.Model):
    __tablename__ = "event"

    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(
        db.String(100),
        nullable=False,
        unique=True
    )

    start_date = db.Column(db.Date)
    start_time = db.Column(db.Time)
    location = db.Column(db.String(120))
