from app.models.event import Event
from app.models.schemas import EventSchema


class EventService:
    @staticmethod
    def get_events():
        # from_date = from_.date()
        # from_time = from_.time()

        # to_date = to_.date()
        # to_time = to_.time()

        # TODO: Filter
        query = Event.query.all()

        data = EventSchema(many=True).dump(query)

        return data
