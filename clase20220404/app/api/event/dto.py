from doctest import debug_script
from flask_restx import Namespace, fields


api = Namespace(
    'Event',
    path='/event',
    description='Event related operations'
)

event = api.model(
    'Event',
    {
        'id': fields.Integer(
            required=True,
            description="Event's identifier"
        ),
        'description': fields.String(
            required=True,
            description='A descr'
        ),
        'start_date': fields.Date(
            required=True,
            description='When'
        )
        # VAMOS POR ACA: TIME
        
    }
)

error_response = api.model(
    'ErrorResponse',
    {
        'code': fields.String,
        'message': fields.String
    }
)
