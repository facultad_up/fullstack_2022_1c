from flask_restx import Resource, Namespace, Model, fields
from flask import Response

# DTO: Data Transfer Object

people = {
    1: {'nombre': 'uno', 'pais': 'One'},
    2: {'nombre': 'dos', 'pais': 'Two'}
}

# archivo: persona_dto.py
person_dto = Model(
    'Person',
    {
        'nro_documento': fields.Integer(
            required=True,
            description='El numero de identificacion de la persona'
        ),
        'nombre': fields.String(
            required=True,
            description='El nombre de la persona'
        ),
        'pais': fields.String(
            description='El pais de origen de la persona'
        ),
        'id_propiedades': fields.List(fields.Integer)
    }
)

request_persona_dto = Model(
    'PersonRequest',
    {
        'nro_documento': fields.Integer(
            required=True,
            description='El numero de identificacion de la persona'
        ),
        'nombre': fields.String(
            required=True,
            description='El nombre de la persona'
        ),
        'pais': fields.String(
            description='El pais de origen de la persona'
        )
    }
)

error_response_dto = Model(
    'ErrorResponse',
    {
        'code': fields.String,
        'message': fields.String
    }
)

api_persona = Namespace(
    'Person',
    path='/people',
    description='People operations'
)

api_persona.model('Person', person_dto)
api_persona.model('ErrorResponse', error_response_dto)
request_person_dto_registred = api_persona.model('PersonRequest', request_persona_dto)


# archivo: persona_resource.py
@api_persona.route('/')
class Persona(Resource):
    @api_persona.doc(
        'Get all people in the database.'
    )
    @api_persona.response(
        200,
        description='Lista de personas',
        model=fields.List(fields.Nested(person_dto))
    )
    @api_persona.response(
        500,
        description='Error en el servidor',
        model=error_response_dto
    )
    def get(self):
        return people

    @api_persona.doc(
        'Incorpora una persona a la base.'
    )
    @api_persona.response(
        201,
        description='Persona creada',
        model=person_dto
    )
    @api_persona.response(
        500,
        description='Error en el servidor',
        model=error_response_dto
    )
    @api_persona.expect(request_person_dto_registred, validate=True)
    def post(self):
        return '201'

# archivo: __init__.py del paquete del endpoint
# Blueprint
from flask import Blueprint
from flask_restx import Api

api_bp = Blueprint('api', __name__)
api = Api(api_bp, title='Backend de personas')
api.add_namespace(api_persona)


# archivo: app.py
from flask import Flask
app = Flask(__name__)
app.register_blueprint(api_bp, url_prefix='/')


app.debug = True
app.run()
