from flask import Flask, Response, request

app = Flask(__name__)

# Dictionary:   key-value
people = {
    1: 'uno',
    2: 'dos'
}


# GET simple
@app.route('/')  # Decorator
def index():
    """The root"""
    return 'Hello World!'

# Listado de personas
@app.route('/people/')
def list_people():
    return people

@app.route('/people/<int:id>')
def get_person(id):
    
    # Revisar que id sea una key valida en el diccionario
    if id in people:
        return people[id]

    return Response(f'Persona no encontrada: {id}', 404)

# ejemplo: curl 'localhost:5000/parametros?a=123&b=456&c=789'
@app.route('/parametros')
def get_algo():
    params = ''
    for x in request.args:
        params += f'{x} = {request.args[x]}\n'
    
    return 'Los parametros son: \n' + params

# APIARY

# curl -i http://127.0.0.1:5000/people/ -X POST -d '{"nro_doc":"123", "nombre":"45"}' -H "Content-type: Application/json"
@app.route('/people/', methods=['POST'])
def add_person():
    print(request.data)
    print(request.form)
    print(request.get_json())
    print(type(request.get_json()))
    
    o = request.get_json()

    if not o or not 'nro_doc' in o:
        return Response('Falta nro_doc', 400)

    people[int(o['nro_doc'])] = o['nombre']

    return Response('Persona creada', 201)


app.debug = True
app.run()
