from flask import Flask, Response, request


app = Flask(__name__)

# Dictionary:   key-value
people = {
    1: 'uno',
    2: 'dos'
}


# GET simple
@app.route('/')  # Decorator
def index():
    """The root"""
    return 'Hello World!'

@app.route('/people/<int:id>')
def get_person(id):
    
    # Revisar que id sea una key valida en el diccionario
    if id in people:
        return people[id]

    return Response(f'Persona no encontrada: {id}', 404)

@app.route('/parametros')
def get_algo():
    params = ''
    for x in request.args:
        params += f'{x} = {request.args[x]}\n'
    
    return 'Los parametros son: \n' + params

app.debug = True
app.run()
